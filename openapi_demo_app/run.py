import connexion


def main():
    app = connexion.FlaskApp(__name__, specification_dir="spec/")
    app.add_api("spec.yaml")
    app.run(port=8080)


if __name__ == "__main__":
    main()
